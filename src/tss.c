/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de estructuras para administrar tareas
*/

#include "tss.h"
#include "mmu.h"


#define USER              0 
#define SYSTEM            1
#define EFLAGS_DEFAULT    0x00000202

tss tss_inicial;
tss tss_idle;

tss tss_jugador[2*MAX_CANT_PERROS_VIVOS];

unsigned short sig_entrada_gdt;

#define COMPLETE_GDT_TSS(I,TSS,DPL)  \
{	gdt[I].limit_0_15 = sizeof(TSS); \
	gdt[I].base_0_15 = ((int) (&TSS)); \
	gdt[I].base_23_16 = (((int) (&TSS)) >> 16); \
	gdt[I].base_31_24 = (((int) (&TSS)) >> 24); \
	gdt[I].type = 0b1001; \
	gdt[I].s = 0; \
	gdt[I].dpl = DPL; \
	gdt[I].p = 1; \
	gdt[I].limit_16_19 = 0x0; \
	gdt[I].avl = 0; \
	gdt[I].l = 0; \
	gdt[I].db = 0; \
	gdt[I].g = 0; \
}

	
/* Esta función llena por primera vez la tss de la tarea
 *  @param _tss    puntero a donde esta la tss
 *  @param system  si es una tarea con privilegio kernel o no (?)
 *  @param eip     dirección de memoria virtual del entry point de la tara
 */
void completar_tss(tss* _tss,char system,uint eip, uint cr3, uint esp, uint ebp) {
	_tss->ptl = 0;
	int* stack_kernel_tarea = dame_pagina_vacia();
	_tss->esp0 = (uint) stack_kernel_tarea + PAGE_SIZE;
	/* _tss->ebp0 = _tss->esp0; */
	_tss->esp1 =  0;
	_tss->esp2 =  0;
	_tss->ss0 = SEL_GDT_DATA_NIVEL0_DESC;
	_tss->ss1 = 0;
	_tss->ss2 = 0;
	_tss->eax = 0x0;
	_tss->ebx = 0x0;
	_tss->ecx = 0x0;
	_tss->edx = 0x0;
	_tss->esi = 0x0;
	_tss->edi = 0x0;
	_tss->eflags = EFLAGS_DEFAULT;
	_tss->esp = esp;
	_tss->ebp = ebp;
	_tss->cs = system ? SEL_GDT_CODE_NIVEL0_DESC : SEL_GDT_CODE_NIVEL3_DESC;
	_tss->ds = system ? SEL_GDT_DATA_NIVEL0_DESC : SEL_GDT_DATA_NIVEL3_DESC;
	_tss->es = system ? SEL_GDT_DATA_NIVEL0_DESC : SEL_GDT_DATA_NIVEL3_DESC;
	_tss->fs = system ? SEL_GDT_DATA_NIVEL0_DESC : SEL_GDT_DATA_NIVEL3_DESC;
	_tss->gs = system ? SEL_GDT_DATA_NIVEL0_DESC : SEL_GDT_DATA_NIVEL3_DESC;
	_tss->ss = system ? SEL_GDT_DATA_NIVEL0_DESC : SEL_GDT_DATA_NIVEL3_DESC;
	_tss->unused0 = 0x0;
	_tss->unused1 = 0x0;
	_tss->unused2 = 0x0;
	_tss->unused3 = 0x0;
	_tss->unused4 = 0x0;
	_tss->unused5 = 0x0;
	_tss->unused6 = 0x0;
	_tss->unused7 = 0x0;
	_tss->unused8 = 0x0;
	_tss->unused9 = 0x0;
	_tss->unused10 = 0x0;
	_tss->eip = eip;
	_tss->cr3 = cr3;
	_tss->iomap = 0xFFFF;
	_tss->ldt = 0;
	_tss->dtrap = 0;
}

void tss_nuevo_perro(perro_t *perro, uint index_jugador, uint index_tipo) {
	tss* _tss = &(tss_jugador[perro->id]);
	uint cr3 = mmu_inicializar_memoria_perro(perro,index_jugador,index_tipo);
	completar_tss(_tss,USER,CODIGO_TAREA_VIRT,cr3,CODIGO_TAREA_VIRT+CODIGO_TAREA_SIZE-12,CODIGO_TAREA_VIRT+CODIGO_TAREA_SIZE-12);
}

//Inicializa la TSS de la tarea idle
void idle_inicializar() {
	completar_tss(&tss_idle,SYSTEM,CODIGO_TAREA_IDLE,DIR_PDE_KERNEL,DIR_EBP_KERNEL,DIR_EBP_KERNEL);
}
	
void tss_inicializar() {
	COMPLETE_GDT_TSS(GDT_TSS_INICIAL_ENTRY,tss_inicial,DPL_KERNEL)
	COMPLETE_GDT_TSS(GDT_TSS_IDLE_ENTRY,tss_idle,DPL_KERNEL)
	uint i=0;
	for(;i<MAX_CANT_PERROS_VIVOS*2;i++) {
		tss* _tss = &(tss_jugador[i]);
		COMPLETE_GDT_TSS(GDT_TSS_IDLE_ENTRY+i+1,(*_tss),DPL_USER)
	}
}

