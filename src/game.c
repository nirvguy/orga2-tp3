/* ** por compatibilidad se omiten tildes **
================================================================================
TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
*/

#include "game.h"
#include "mmu.h"
#include "tss.h"
#include "screen.h"

#include <stdarg.h>


int escondites[ESCONDITES_CANTIDAD][3] = { // TRIPLAS DE LA FORMA (X, Y, HUESOS)
                                        {76,  25, 50}, {12, 15, 50}, {9, 10, 100}, {47, 21, 100} ,
                                        {34,  11, 50}, {75, 38, 50}, {40, 21, 100}, {72, 17, 100}
                                    };

jugador_t jugadorA;
jugador_t jugadorB;
orden_t ultima_ordenA;
orden_t ultima_ordenB;

short* mem_video = (short*)0xB8000;
short bk_video[VIDEO_FILS*VIDEO_COLS];
perro_t *game_perro_actual = NULL;
int ultimo_cambio = MAX_SIN_CAMBIOS;
int cant_huesos = 0;
e_modo_juego modo_juego = NORMAL;

void ASSERT_OR_ERROR(uint value, char* error_msg)
{
	if (!value) {
		print(error_msg, 5, 5, C_BG_LIGHT_GREY | C_FG_BLACK);
		breakpoint();
	}
}

void* error()
{
	__asm__ ("int3");
	return 0;
}

uint game_xy2lineal (uint x, uint y) {
	return (y * MAPA_ANCHO + x);
}

uint game_es_posicion_valida(int x, int y) {
	return (x >= 0 && y >= 0 && x < MAPA_ANCHO && y < MAPA_ALTO);
}


void game_inicializar()
{
	modo_juego = NORMAL;
	game_jugador_inicializar(&jugadorA);
	game_jugador_inicializar(&jugadorB);

	int i=0;
	for(;i<ESCONDITES_CANTIDAD;i++)
		cant_huesos += escondites[i][2];

    screen_pintar_puntajes();
}


// devuelve la cantidad de huesos que hay en la posición pasada como parametro
uint game_huesos_en_posicion(uint x, uint y)
{
	int i;
	for (i = 0; i < ESCONDITES_CANTIDAD; i++)
	{
		if (escondites[i][0] == x && escondites[i][1] == y)
			return escondites[i][2];
	}
	return 0;
}




// devuelve algun perro que esté en la posicion pasada (hay max 2, uno por jugador)
perro_t* game_perro_en_posicion(uint x, uint y)
{
	int i;
	for (i = 0; i < MAX_CANT_PERROS_VIVOS; i++)
	{
		if (!jugadorA.perros[i].libre && jugadorA.perros[i].x == x && jugadorA.perros[i].y == y)
			return &jugadorA.perros[i];
		if (!jugadorB.perros[i].libre && jugadorB.perros[i].x == x && jugadorB.perros[i].y == y)
			return &jugadorB.perros[i];
	}
	return NULL;
}



// termina si se agotaron los huesos o si hace tiempo que no hay ningun cambio
void game_terminar_si_es_hora()
{
	if(cant_huesos == 0) {
		if(jugadorA.puntos > jugadorB.puntos)
			screen_stop_game_show_winner(&jugadorA);
		else
			screen_stop_game_show_winner(&jugadorB);
	}
}

void restaurar_pantalla() {
	int i=0;

	for(i=0;i<VIDEO_FILS*VIDEO_COLS;i++)
			mem_video[i] = bk_video[i];
	

}

void game_modo_debug(uint gs,
					 uint es,
					 uint cs,
					 uint ds,
					 uint fs,
					 uint ss,
					 uint flags,
					 uint edi,
					 uint esi,
					 uint ebp,
					 uint esp,
					 uint ebx,
					 uint edx,
					 uint ecx,
					 uint eax,
					 uint eip) {
		
	int i=0;
	for(i=0;i<VIDEO_FILS*VIDEO_COLS;i++)
			bk_video[i] = mem_video[i];

	screen_pintar_rect(' ',C_BG_BLACK | C_FG_BLACK,1,25,41,40);
	screen_pintar_linea_h(' ',C_BG_RED | C_FG_BLACK,2,26,38);
	screen_pintar_rect(' ',C_BG_LIGHT_GREY | C_FG_BLACK,3,26,38,38);
	print("eax:",27,4,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("ebx:",27,6,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("ecx:",27,8,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("edx:",27,10,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("esi:",27,12,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("edi:",27,14,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("ebp:",27,16,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("esp:",27,18,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("eip:",27,20,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("cs:",27,22,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("ds:",27,24,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("es:",27,26,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("fs:",27,28,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("gs:",27,30,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("ss:",27,32,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("eflags:",27,34,C_BG_LIGHT_GREY | C_FG_BLACK);
	print_hex(eax,8,31,4,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(ebx,8,31,6,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(ecx,8,31,8,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(edx,8,31,10,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(esi,8,31,12,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(edi,8,31,14,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(ebp,8,31,16,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(esp,8,31,18,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(eip,8,31,20,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(cs,4,35,22,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(ds,4,35,24,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(es,4,35,26,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(fs,4,35,28,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(gs,4,35,30,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(ss,4,35,32,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(flags,8,34,34,C_BG_LIGHT_GREY | C_FG_WHITE);
	uint cr0 = rcr0();
	uint cr2 = rcr2();
	uint cr3 = rcr3();
	uint cr4 = rcr4();
	print("cr0:",40,4,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("cr2:",40,6,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("cr3:",40,8,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("cr4:",40,10,C_BG_LIGHT_GREY | C_FG_BLACK);
	print("stack:",40,12,C_BG_LIGHT_GREY | C_FG_BLACK);
	print_hex(cr0,8,45,4,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(cr2,8,45,6,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(cr3,8,45,8,C_BG_LIGHT_GREY | C_FG_WHITE);
	print_hex(cr4,8,45,10,C_BG_LIGHT_GREY | C_FG_WHITE);
	uint* stack = (uint*)esp-8;
	for(i=0;i<=8;i++)
		print_hex(stack[i],8,45,14+2*i,C_BG_LIGHT_GREY | C_FG_WHITE);
}




