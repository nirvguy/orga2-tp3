; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================

extern GDT_DESC
extern IDT_DESC
extern idt_inicializar
extern game_inicializar
extern screen_inicializar
extern idle_inicializar
extern tss_inicializar
extern sched_inicializar
extern mmu_inicializar
extern mmu_inicializar_dir_kernel
extern resetear_pic
extern habilitar_pic

%include "imprimir.mac"

%define DIR_MODO_PROTEGIDO          (8 << 3)
%define DIR_REG_SEGMENTS 	    (10 << 3)

%define SEL_TSS_INICIAL         (12 << 3)
%define SEL_TSS_IDLE            (13 << 3)

global start


;; Saltear seccion de datos
jmp start

;;
;; Seccion de datos.
;; -------------------------------------------------------------------------- ;;
iniciando_mr_msg db     'Iniciando kernel (Modo Real)...'
iniciando_mr_len equ    $ - iniciando_mr_msg

iniciando_mp_msg db     'Iniciando kernel (Modo Protegido)...'
iniciando_mp_len equ    $ - iniciando_mp_msg

nombre_grupo db 'Nombre GRUPO'
nombre_grupo_len equ $ - nombre_grupo

;;
;; Seccion de código.
;; -------------------------------------------------------------------------- ;;

;; Punto de entrada del kernel.
BITS 16
start:
    ; Deshabilitar interrupciones
    cli

    ; Cambiar modo de video a 80 X 50
    mov ax, 0003h
    int 10h ; set mode 03h
    xor bx, bx
    mov ax, 1112h
    int 10h ; load 8x8 font

    ; Imprimir mensaje de bienvenida
    imprimir_texto_mr iniciando_mr_msg, iniciando_mr_len, 0x07, 0, 0


    ; Habilitar A20
    call habilitar_A20

    ; Cargar la GDT
	lgdt [GDT_DESC]

    ; Setear el bit PE del registro CR0
	mov eax, CR0
	or eax, 1
	mov cr0, eax

    ; Saltar a modo protegido
	jmp DIR_MODO_PROTEGIDO:modoProtegido

BITS 32

modoProtegido:
	
	xchg bx, bx
	
    ; Establecer selectores de segmentos
	
	mov ax, DIR_REG_SEGMENTS
	mov ds, ax
	mov es, ax
	mov gs, ax
	mov fs, ax
	mov ss, ax

    ; Establecer la base de la pila

	mov ebp, 0x27000
	mov esp, ebp
	

    ; Imprimir mensaje de bienvenida
    imprimir_texto_mp iniciando_mp_msg, iniciando_mp_len, 0x07, 0, 0

    ; Inicializar el juego
	call game_inicializar ;falta testear, funcion sacada de game.c

    ; Inicializar pantalla
	call screen_inicializar ;ejercicio 3.a falta testear sacada de screen.c

    ; Inicializar el manejador de memoria
	call mmu_inicializar

    ; Inicializar el directorio de paginas
	call mmu_inicializar_dir_kernel

    ; Cargar directorio de paginas
	mov cr3, eax

    ; Habilitar paginacion
	mov eax, cr0
	or eax, 0x80000000 ;seteamos el bit PG del CR0 para habilitar paginacion
	mov cr0, eax ;se habilita paginacion
	
	; Impresión nombre del grupo
    imprimir_texto_mp nombre_grupo, nombre_grupo_len, 0x07, 0, 0

    ; Inicializar tss
	call tss_inicializar

    ; Inicializar tss de la tarea Idle
	call idle_inicializar

    ; Inicializar el scheduler
	call sched_inicializar

    ; Inicializar la IDT
	call idt_inicializar

    ; Cargar IDT
 	lidt [IDT_DESC]

    ;Testeo de la IDT forzando una interrupcion del tipo DE
    ;	xor eax, eax
    ;	xor ebx, ebx
    ;	div bx

    ; Configurar controlador de interrupciones
	call resetear_pic
	call habilitar_pic

    ; Cargar tarea inicial
	mov ax, SEL_TSS_INICIAL
	ltr ax

    ; Habilitar interrupciones
	sti

    ; Saltar a la primera tarea: Idle
	jmp SEL_TSS_IDLE:0

    ; Ciclar infinitamente (por si algo sale mal...)

    mov eax, 0xFFFF
    mov ebx, 0xFFFF
    mov ecx, 0xFFFF
    mov edx, 0xFFFF

    jmp $
    jmp $

;; -------------------------------------------------------------------------- ;;

%include "a20.asm"
