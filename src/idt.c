
/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de las rutinas de atencion de interrupciones
*/

#include "defines.h"
#include "idt.h"
#include "isr.h"

#include "tss.h"
#
idt_entry idt[255] = { };

idt_descriptor IDT_DESC = {
    sizeof(idt) - 1,
    (unsigned int) &idt
};


#define IDT_ENTRY(numero, dpl)                                                                                   \
    idt[numero].offset_0_15 = (unsigned short) ((unsigned int)(&_isr ## numero) & (unsigned int) 0xFFFF);        \
    idt[numero].segsel = (unsigned short) (SEL_GDT_CODE_NIVEL0_DESC); \
    idt[numero].attr = (unsigned short) 0b1000111000000000 | (((unsigned short)(dpl & 0x3)) << 13);                          \
    idt[numero].offset_16_31 = (unsigned short) ((unsigned int)(&_isr ## numero) >> 16 & (unsigned int) 0xFFFF)



void idt_inicializar() {
    // Excepciones
        IDT_ENTRY(0,  DPL_KERNEL);
        IDT_ENTRY(1,  DPL_KERNEL);
        IDT_ENTRY(2,  DPL_KERNEL);
        IDT_ENTRY(3,  DPL_KERNEL);
        IDT_ENTRY(4,  DPL_KERNEL);
        IDT_ENTRY(5,  DPL_KERNEL);
        IDT_ENTRY(6,  DPL_KERNEL);
        IDT_ENTRY(7,  DPL_KERNEL);
        IDT_ENTRY(8,  DPL_KERNEL);
        IDT_ENTRY(10, DPL_KERNEL);
        IDT_ENTRY(11, DPL_KERNEL);
        IDT_ENTRY(12, DPL_KERNEL);
        IDT_ENTRY(13, DPL_KERNEL);
        IDT_ENTRY(14, DPL_KERNEL);
        IDT_ENTRY(16, DPL_KERNEL);
        IDT_ENTRY(17, DPL_KERNEL);
        IDT_ENTRY(18, DPL_KERNEL);
        IDT_ENTRY(19, DPL_KERNEL);
        IDT_ENTRY(32, DPL_KERNEL);
        IDT_ENTRY(33, DPL_KERNEL);
	// Syscalls
        IDT_ENTRY(70, DPL_USER);

}
