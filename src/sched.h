/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del scheduler
*/

#ifndef __SCHED_H__
#define __SCHED_H__

#include "game.h"

#define MAX_CANT_TAREAS_VIVAS   (MAX_CANT_PERROS_VIVOS * 2)

extern perro_t perro_iddle; //informe expliocar perro iddle
/*
================================================================================
                       ~~~ estructuras del scheduler ~~~
================================================================================
*/

// para el scheduler, lo importante de cada tarea es su indice en la gdt y
// el perro al cual está asociada. Si el indice es null significará que está libre.
typedef struct sched_task_t
{
    unsigned int gdt_index;
	perro_t *perro;
} sched_task_t;

// el scheduler posee un arreglo de tareas (cada una puede estar libre o asignada)
typedef struct sched_t
{
    sched_task_t tasks[MAX_CANT_TAREAS_VIVAS+1];
    ushort current;
	ushort perro_other;
    ushort jugador_current;
} sched_t;

extern sched_t scheduler;

/*
================================================================================
                       ~~~ funciones del scheduler ~~~
================================================================================
*/
// Iniciliza la estructura del scheduler
void sched_inicializar();

// Mata la tarea que se está ejecutando actualmente
void sched_matar_actual();

// Devuelve el puntero al perro actual que se esta ejecutando
perro_t* sched_tarea_actual();

// debe avisar al juego que hubo un tick (para que haga cosas del juego) y luego configurarse
// para pasar a la siguiente tarea (devuelve su indice en la gdt)
ushort sched_atender_tick();

#endif	/* !__SCHED_H__ */
