/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de la tabla de descriptores globales
*/

#include "gdt.h"




/* Definicion de la GDT */
/* -------------------------------------------------------------------------- */

gdt_entry gdt[GDT_COUNT] = {
    /* Descriptor nulo*/
    /* Offset = 0x00 */
    [GDT_IDX_NULL_DESC] = (gdt_entry) {
        (unsigned short)    0x0000,         /* limit[0:15]  */
        (unsigned short)    0x0000,         /* base[0:15]   */
        (unsigned char)     0x00,           /* base[23:16]  */
        (unsigned char)     0x00,           /* type         */
        (unsigned char)     0x00,           /* s            */
        (unsigned char)     0x00,           /* dpl          */
        (unsigned char)     0x00,           /* p            */
        (unsigned char)     0x00,           /* limit[16:19] */
        (unsigned char)     0x00,           /* avl          */
        (unsigned char)     0x00,           /* l            */
        (unsigned char)     0x00,           /* db           */
        (unsigned char)     0x00,           /* g            */
        (unsigned char)     0x00,           /* base[31:24]  */

	},
    /* Descriptor Codigo Nivel 0*/
    /* Offset = 0x00 */
    [GDT_CODE_NIVEL0_DESC] = (gdt_entry) {
        (unsigned short)    SEGMENT_LIMIT_LO,/* limit[0:15]  */
        (unsigned short)    0x0000,         /* base[0:15]   */
        (unsigned char)     0x00,           /* base[23:16]  */
        (unsigned char)     0x0A,           /* type         */
        (unsigned char)     GDT_ATTR_CODE,  /* s            */
        (unsigned char)     DPL_KERNEL,     /* dpl          */
        (unsigned char)     TRUE,           /* p            */
        (unsigned char)     SEGMENT_LIMIT_HI,/* limit[16:19] */
        (unsigned char)     0x00,           /* avl          */
        (unsigned char)     0x00,           /* l            */
        (unsigned char)     0x01,           /* db           */
        (unsigned char)     0x01,           /* g            */
        (unsigned char)     0x00,           /* base[31:24]  */

	},

    /* Descriptor Datos Nivel 0*/
    /* Offset = 0x00 */
    [GDT_DATA_NIVEL0_DESC] = (gdt_entry) {
        (unsigned short)    SEGMENT_LIMIT_LO,  /* limit[0:15]  */
        (unsigned short)    0x0000,         /* base[0:15]   */
        (unsigned char)     0x00,           /* base[23:16]  */
        (unsigned char)     0x02,           /* type         */
        (unsigned char)     GDT_ATTR_DATA,  /* s            */
        (unsigned char)     DPL_KERNEL,     /* dpl          */
        (unsigned char)     TRUE,           /* p            */
        (unsigned char)     SEGMENT_LIMIT_HI,/* limit[16:19] */
        (unsigned char)     0x00,           /* avl          */
        (unsigned char)     0x00,           /* l            */
        (unsigned char)     0x01,           /* db           */
        (unsigned char)     0x01,           /* g            */
        (unsigned char)     0x00,           /* base[31:24]  */
	
	},

    /* Descriptor Codigo Nivel 0*/
    /* Offset = 0x00 */
    [GDT_CODE_NIVEL3_DESC] = (gdt_entry) {
        (unsigned short)    SEGMENT_LIMIT_LO,  /* limit[0:15]  */
        (unsigned short)    0x0000,         /* base[0:15]   */
        (unsigned char)     0x00,           /* base[23:16]  */
        (unsigned char)     0x0A,           /* type         */
        (unsigned char)     GDT_ATTR_CODE,  /* s            */
        (unsigned char)     DPL_USER,       /* dpl          */
        (unsigned char)     TRUE,           /* p            */
        (unsigned char)     SEGMENT_LIMIT_HI,/* limit[16:19] */
        (unsigned char)     0x00,           /* avl          */
        (unsigned char)     0x00,           /* l            */
        (unsigned char)     0x01,           /* db           */
        (unsigned char)     0x01,           /* g            */
        (unsigned char)     0x00,           /* base[31:24]  */

	},

    /* Descriptor Codigo Datos 3*/
    /* Offset = 0x00 */
    [GDT_DATA_NIVEL3_DESC] = (gdt_entry) {
        (unsigned short)    SEGMENT_LIMIT_LO, /* limit[0:15]  */
        (unsigned short)    0x0000,         /* base[0:15]   */
        (unsigned char)     0x00,           /* base[23:16]  */
        (unsigned char)     0x02,           /* type         */
        (unsigned char)     GDT_ATTR_DATA,  /* s            */
        (unsigned char)     DPL_USER,       /* dpl          */
        (unsigned char)     TRUE,           /* p            */
        (unsigned char)     SEGMENT_LIMIT_HI,/* limit[16:19] */
        (unsigned char)     0x00,           /* avl          */
        (unsigned char)     0x00,           /* l            */
        (unsigned char)     0x01,           /* db           */
        (unsigned char)     0x01,           /* g            */
        (unsigned char)     0x00,           /* base[31:24]  */
	}

};

gdt_descriptor GDT_DESC = {
    sizeof(gdt) - 1,
    (unsigned int) &gdt
};
