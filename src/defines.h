/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

    Definiciones globales del sistema.
* -------------------------------------------------------------------------- */

#ifndef __DEFINES_H__
#define __DEFINES_H__

/* Tipos basicos */
/* -------------------------------------------------------------------------- */
#define NULL    0
#define TRUE    0x00000001
#define FALSE   0x00000000
#define ERROR   1

typedef unsigned char  uchar;
typedef unsigned short ushort;
typedef unsigned int   uint;
typedef unsigned char  bool;

/* Constantes basicas */
/* -------------------------------------------------------------------------- */
#define PAGE_SIZE    0x00001000
#define TASK_SIZE    4096

#define BOOTSECTOR   0x00001000   /*   direccion fisica de comienzo del bootsector (copiado)   */
#define KERNEL       0x00001200   /*   direccion fisica de comienzo del kernel                 */

#define SEGMENT_LIMIT  0x1F3FF
#define SEGMENT_LIMIT_LO  SEGMENT_LIMIT & 0xFFFF
#define SEGMENT_LIMIT_HI  (SEGMENT_LIMIT >> 16) & 0xF


/* Indices en la gdt */
/* -------------------------------------------------------------------------- */
#define GDT_COUNT              30

#define DPL_KERNEL             0
#define DPL_USER               3

#define GDT_IDX_NULL_DESC      0
#define GDT_CODE_NIVEL0_DESC   8   /* N° entrada de la gdt del segmento flat de codigo de nivel kernel */
#define GDT_CODE_NIVEL3_DESC   9   /* N° entrada de la gdt del segmento flat de codigo de nivel usuario */
#define GDT_DATA_NIVEL0_DESC   10  /* N° entrada de la gdt del segmento flat de datos  de nivel kernel */
#define GDT_DATA_NIVEL3_DESC   11  /* N° entrada de la gdt del segmento flat de datos  de nivel usuario */
#define GDT_TSS_INICIAL_ENTRY  12
#define GDT_TSS_IDLE_ENTRY     13

/* Selectores de segmentos */
/* -------------------------------------------------------------------------- */
#define GDT_OFF_NULL_DESC          GDT_IDX_NULL_DESC      <<   3                 /*    */
#define SEL_GDT_CODE_NIVEL0_DESC   GDT_CODE_NIVEL0_DESC   <<   3
#define SEL_GDT_CODE_NIVEL3_DESC   GDT_CODE_NIVEL3_DESC   <<   3   |   DPL_USER
#define SEL_GDT_DATA_NIVEL0_DESC   GDT_DATA_NIVEL0_DESC   <<   3
#define SEL_GDT_DATA_NIVEL3_DESC   GDT_DATA_NIVEL3_DESC   <<   3   |   DPL_USER
#define SEL_GDT_TSS_INICIAL_ENTRY  GDT_TSS_INICIAL_ENTRY  <<   3
#define SEL_GDT_TSS_IDLE_ENTRY     GDT_TSS_IDLE_ENTRY     <<   3

#define DIR_MODO_PROTEGIDO         SEL_GDT_CODE_NIVEL0_DESC

/* Direcciones fisicas de directorios y tablas de paginas del KERNEL */
/* -------------------------------------------------------------------------- */
#define DIR_PAG_LIBRE    0x100000  /* Dirección fisica donde comienzan las paginas libres */
#define DIR_PDE_KERNEL   0x27000   /* Dirección fisica donde se guarda el directorio de paginas del kernel */
#define DIR_PTE_KERNEL   0x28000   /* Dirección fiscia de la tabla de paginas asociada a la primer entrada del directorio de paginas del kernel */
#define DIR_EBP_KERNEL   0x27000   /* Dirección fisica donde se almancena el base pointer del kernel (crece numericamente para abajo, no se pisa con la pde) */

/* Codigo orginal de las tareas */
/* -------------------------------------------------------------------------- */

#define CODIGO_TAREA_A1     0x10000
#define CODIGO_TAREA_A2     0x11000
#define CODIGO_TAREA_B1     0x12000
#define CODIGO_TAREA_B2     0x13000
#define CODIGO_TAREA_IDLE   0x16000

#define CODIGO_TAREA_SIZE   PAGE_SIZE

#define CODIGO_TAREA_VIRT   0x401000  /* Dirección virtual donde estará el codigo de la tarea (mapeado fisicamente a la posición del mapa donde esta la tarea) */
#define COMPARTIDA_VIRT     0x400000  /* Dirección virtual donde las tareas tienen mapeado una pagina de 4K en comun entre todas */

#define MAPA_TEMP           0x402000

/* Auxiliares del mapa */
/* -------------------------------------------------------------------------- */
#define MAPA_BASE_FISICA                0x500000  /* Dirección virtual donde esta mapeado el orden fisicamente el mapa */
#define MAPA_BASE_VIRTUAL               0x800000  /* Dirección fisica donde comienza el mapa */

#define MAPA_ANCHO                       80
#define MAPA_ALTO                        44
#define CELDA_SIZE                      PAGE_SIZE

/* Traduce fila y columna a la dirección fisica de la celda del mapa que le corresponde a esa posición */
#define COORD_TO_FISICA(FILA,COLUMNA)   MAPA_BASE_FISICA  + ((FILA) * MAPA_ANCHO + COLUMNA) * CELDA_SIZE
/* Traduce fila y columna a la dirección virtual de la celda del mapa que le corresponde a esa posición */
#define COORD_TO_VIRT(FILA,COLUMNA)     MAPA_BASE_VIRTUAL + ((FILA) * MAPA_ANCHO + COLUMNA) * CELDA_SIZE

#define MAPA_SALIDA_JUGADOR_A_FISICA    COORD_TO_FISICA(1,1)
#define MAPA_SALIDA_JUGADOR_B_FISICA    COORD_TO_FISICA((MAPA_ALTO-2),(MAPA_ANCHO-2))

#define MAPA_SALIDA_JUGADOR_A_VIRT      COORD_TO_VIRT(1,1)
#define MAPA_SALIDA_JUGADOR_B_VIRT      COORD_TO_VIRT((MAPA_ALTO-2),(MAPA_ANCHO-2))

/* Auxiliares de la mmu */
/* -------------------------------------------------------------------------- */
#define TABLE_ENTRIES           1024

#define PRESENT                  0x1

#define MMU_ATTR_DISABLED        0x0
#define MMU_ATTR_PRESENT         PRESENT
#define MMU_ATTR_READ_WRITE      0x2
#define MMU_ATTR_SUPERVISOR      0x0
#define MMU_ATTR_USER            0x4

/* Auxiliares de la GDT
 *--------------------------------------------------------------------------- */

#define GDT_ATTR_SYSTEM          0x0
#define GDT_ATTR_CODE            0x1
#define GDT_ATTR_DATA            0x1

/* Codigo de las syscalls
 *---------------------------------------------------------------------------- */

#define SYSCALL_MOVERSE          0x1
#define SYSCALL_CAVAR            0x2
#define SYSCALL_OLFATEAR         0x3
#define SYSCALL_RECIBIR_ORDEN    0x4


#endif  /* !__DEFINES_H__ */
