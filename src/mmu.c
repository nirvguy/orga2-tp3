/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del manejador de memoria
*/

#include "mmu.h"
#include "i386.h"
/* Atributos paginas */
/* -------------------------------------------------------------------------- */


/* Direcciones fisicas de codigos */
/* -------------------------------------------------------------------------- */
/* En estas direcciones estan los códigos de todas las tareas. De aqui se
 * copiaran al destino indicado por TASK_<i>_CODE_ADDR.
 */



#define UINT_TO_VIRTUAL(V,L)  L.offset = V & 0xFFF; \
                             L.table = (V >> 12) & 0x3FF; \
                             L.directory = (V >> 22) & 0x3FF

#define UINT_TO_CR3(C,S)     S.f_ignored = C & 0x7; \
                             S.cache_attr = (C >> 3) & 0x3; \
                             S.s_ignored = (C >> 5) & 0x7F; \
                             S.dir_base_pd = (C >> 12) & 0xFFFFF

void mmu_mapear_pagina  (uint virtual, uint cr3, uint fisica, uint attrs)
{
	CR3 _cr3;
	dir_lineal virt;
	UINT_TO_VIRTUAL(virtual,virt);
	UINT_TO_CR3(cr3,_cr3);
	// Calcula la dirección lineal de la entrada en el directorio de paginas
	str_pd_entry* addr_pde =  (str_pd_entry*) ((_cr3.dir_base_pd << 12) +
	virt.directory * sizeof(str_pd_entry));
	// Si no esta presente, reserva una pagina vacía nueva mapeando en la
	// entrada de la pde esta nueva pagina
	if(!(addr_pde->attr & MMU_ATTR_PRESENT)) {
		void* pag_vacia = dame_pagina_vacia();
		addr_pde->attr = (attrs & 0xFFF);
		addr_pde->dir_base = (((int) pag_vacia) >> 12);
	}
	// Calcula la dirección lineal de la entrada de la tabla de paginas y la
	// setea con la dirección base y attributos correspondiente
	str_pt_entry* addr_pte = (str_pt_entry*) ((addr_pde->dir_base << 12) +
	virt.table * sizeof(str_pt_entry));
	addr_pte->dir_base = (fisica >> 12);
	addr_pte->attr = (attrs & 0xFFF);
	tlbflush();
}


void mmu_unmapear_pagina(uint virtual, uint cr3) {
	CR3 _cr3;
	dir_lineal virt;
	UINT_TO_VIRTUAL(virtual,virt);
	UINT_TO_CR3(cr3,_cr3);
	str_pd_entry* addr_pde =  (str_pd_entry*) ((_cr3.dir_base_pd << 12) + virt.directory * sizeof(str_pd_entry));
	str_pt_entry* addr_pte = (str_pt_entry*) ((addr_pde->dir_base << 12) + virt.table * sizeof(str_pt_entry));
	addr_pte->attr = MMU_ATTR_DISABLED;
	tlbflush();
}

void* dame_pagina_vacia() {
	int* pagina_libre = (int*) (DIR_PAG_LIBRE+offset_sig_pag_libre);
	int i=0;
	// Vacia la pagina
	for(;i<(PAGE_SIZE << 2);i++)
		pagina_libre[i] = 0;
	offset_sig_pag_libre += PAGE_SIZE;
	return (void*) pagina_libre;
}

void identity_mapping(str_pt_entry* pte, uint attr) {
	int i=0;
	for(;i<TABLE_ENTRIES;i++){
		pte[i].attr = attr & 0xFFF;
		pte[i].dir_base = i;
	}
}

uint mmu_inicializar_dir_kernel(){
	short i = 0;
	// Marca como no presente todas las entradas del directorio de paginas
	// salvo la 0 que es la del kernel
	str_pd_entry* pdeReg =  (str_pd_entry*) DIR_PDE_KERNEL;
	for(;i<TABLE_ENTRIES;i++){
		pdeReg[i].attr = MMU_ATTR_READ_WRITE;
	}
	pdeReg[0].attr = MMU_ATTR_READ_WRITE | MMU_ATTR_PRESENT;
	pdeReg[0].dir_base = (DIR_PTE_KERNEL >> 12);
	str_pt_entry* pteReg = (str_pt_entry*) DIR_PTE_KERNEL;

	// Realiza identity mapping con los primeros 4MB
	identity_mapping(pteReg, MMU_ATTR_READ_WRITE | MMU_ATTR_PRESENT);

	//void* video = (void*) (DIR_PTE+512*4); CONSULTAR!
	//video = 0x000B8003;	
	return DIR_PDE_KERNEL;
}

void mmu_inicializar() {
	offset_sig_pag_libre = 0;
	// se reserva un nuevo directorio de paginas
	// y se mapea la primer entrada con la del kernel
	pagina_tarea_compartida= dame_pagina_vacia();
	//habria que devolver la dir_pagina_vacia, no?
}

void mmu_remapear_perro(perro_t* perro) {
	uint cr3_actual = rcr3();
	uint fisica = COORD_TO_FISICA(perro->x,perro->y);
	mmu_mapear_pagina(COORD_TO_VIRT(perro->x,perro->y), cr3_actual, fisica, MMU_ATTR_USER | MMU_ATTR_PRESENT);
	mmu_mapear_pagina(MAPA_TEMP, cr3_actual,fisica, MMU_ATTR_READ_WRITE | MMU_ATTR_PRESENT | MMU_ATTR_USER);
	int *dir_mapa = (int*) (MAPA_TEMP);
	int *codigo = (int*) CODIGO_TAREA_VIRT;
	int i=0;
	for(;i<(CODIGO_TAREA_SIZE >> 2);i++) 
		dir_mapa[i] = codigo[i];
	mmu_mapear_pagina(CODIGO_TAREA_VIRT, cr3_actual, fisica, MMU_ATTR_USER | MMU_ATTR_READ_WRITE | MMU_ATTR_PRESENT);
	mmu_unmapear_pagina(MAPA_TEMP,cr3_actual);
}


uint mmu_inicializar_memoria_perro(perro_t *perro, int index_jugador, int index_tipo) {
	/*******************************************************/
	/***         Mapeo de paginas de la tearea nueva     ***/
	/*******************************************************/
	str_pd_entry* pd_tarea = (str_pd_entry*) dame_pagina_vacia();
	str_pt_entry* pt_tarea_kernel = (str_pt_entry*) dame_pagina_vacia();

	// Llena la primer entrada de la page directory nueva de la terea haciendo
	// identity mapping con los primeros 4MB del kernel
	pd_tarea[0].dir_base = (((int) pt_tarea_kernel) >> 12);
	pd_tarea[0].attr = MMU_ATTR_PRESENT;
	identity_mapping(pt_tarea_kernel, MMU_ATTR_PRESENT);

	// Crea una entrada en la PDT de la tarea para el codigo
	uint cr3 = (uint) pd_tarea;
	uint fisica;
	uint virtual;
	switch(index_jugador) {
		case 0: 
			fisica = MAPA_SALIDA_JUGADOR_A_FISICA;
			virtual = MAPA_SALIDA_JUGADOR_A_VIRT;
			break;
		case 1: 
			fisica = MAPA_SALIDA_JUGADOR_B_FISICA;
			virtual = MAPA_SALIDA_JUGADOR_B_VIRT;
			break;
	}
	//Mapea donde va a estar el codigo de la tarea en la direccion virtual CODIGO_TAREA_VIRT
	mmu_mapear_pagina(CODIGO_TAREA_VIRT, cr3, fisica, MMU_ATTR_USER | MMU_ATTR_READ_WRITE | MMU_ATTR_PRESENT);
	//Mapea la pagina fisica compartida que tienen todas las tareas en la direccion virtual COMPARTIDA_VIRT
	mmu_mapear_pagina(COMPARTIDA_VIRT, cr3, (uint) pagina_tarea_compartida, MMU_ATTR_USER | MMU_ATTR_READ_WRITE | MMU_ATTR_PRESENT);
	//mapea la pagina que va teniendo la pagina que va recorriendo
	mmu_mapear_pagina(virtual, cr3, fisica, MMU_ATTR_PRESENT | MMU_ATTR_USER);
	
	/*******************************************************/
	/***      Copia de codigo en el lugar del mapa       ***/
	/*******************************************************/

	// Calcula la direcciòn virtual de donde esta el codigo de la tarea original (virtual)
	int* codigo_tarea_original;
	switch(index_jugador) {
		case 0:
			switch(index_tipo) {
				case 0: codigo_tarea_original = (int*) CODIGO_TAREA_A1; break;
				case 1: codigo_tarea_original = (int*) CODIGO_TAREA_A2; break;
			}
			break;
		case 1:
			switch(index_tipo) {
				case 0: codigo_tarea_original = (int*) CODIGO_TAREA_B1; break;
				case 1: codigo_tarea_original = (int*) CODIGO_TAREA_B2; break;
			}
			break;
	}

	uint cr3_actual = rcr3();
	mmu_mapear_pagina(MAPA_TEMP,cr3_actual,fisica,MMU_ATTR_READ_WRITE | MMU_ATTR_PRESENT | MMU_ATTR_USER);

	// Copiamos el codigo
	int i=0;
	int *dir_mapa = (int*) (MAPA_TEMP);
	

	for(;i<(CODIGO_TAREA_SIZE >> 2);i++) 
		dir_mapa[i] = codigo_tarea_original[i];
		
	// Ponerle en el espacio de stack que es el mismo que el codigo
	// los argumentos de la tarea (que son x_origen e y_origen?)
	switch(index_jugador) {
		case 0:
			dir_mapa[(CODIGO_TAREA_SIZE/4)-1] = 1;
			dir_mapa[(CODIGO_TAREA_SIZE/4)-2] = 1;
			break;
		case 1:
			dir_mapa[(CODIGO_TAREA_SIZE/4)-1] = MAPA_ALTO-1;
			dir_mapa[(CODIGO_TAREA_SIZE/4)-2] = MAPA_ANCHO-1;
			break;
	}

	mmu_unmapear_pagina(MAPA_TEMP, cr3_actual);
	

	return cr3;
}
