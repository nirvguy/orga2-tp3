; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================
; definicion de rutinas de atencion de interrupciones

%include "imprimir.mac"

extern game_atender_teclado
extern game_syscall_manejar
extern game_perro_termino
extern screen_actualizar_reloj_global
extern game_tecla
extern scheduler
extern modo_juego
extern game_modo_debug

BITS 32


sched_tarea_offset:     dd 0x00
sched_tarea_selector:   dw 0x00

syscall_retorno:        dd 0x00

;; PIC
extern fin_intr_pic1

;; Sched
extern sched_atender_tick
extern sched_tarea_actual

msg_isr_0: db 'Error de division'
msg_isr_len_0 EQU $ - msg_isr_0

msg_isr_1: db 'Reservado por Intel'
msg_isr_len_1 EQU $ - msg_isr_1

msg_isr_2: db 'Interrupcion no enmascarable'
msg_isr_len_2 EQU $ - msg_isr_2

msg_isr_3: db 'Breakpoint Trap'
msg_isr_len_3 EQU $ - msg_isr_3

msg_isr_4: db 'Overflow'
msg_isr_len_4 EQU $ - msg_isr_4

msg_isr_5: db 'rango de BOUND extendido'
msg_isr_len_5 EQU $ - msg_isr_5

msg_isr_6: db 'Opcode invalido o indefinido'
msg_isr_len_6 EQU $ - msg_isr_6

msg_isr_7: db 'Dispositivo matemiatico no disponible'
msg_isr_len_7 EQU $ - msg_isr_7

msg_isr_8: db 'Doble falta'
msg_isr_len_8 EQU $ - msg_isr_8

msg_isr_10: db 'TSS Invalido'
msg_isr_len_10 EQU $ - msg_isr_10

msg_isr_11: db 'Segmento no presente'
msg_isr_len_11 EQU $ - msg_isr_11

msg_isr_12: db 'Falta en el Stack-Segment'
msg_isr_len_12 EQU $ - msg_isr_12

msg_isr_13: db 'General Protection'
msg_isr_len_13 EQU $ - msg_isr_13

msg_isr_14: db 'Pagina no presente'
msg_isr_len_14 EQU $ - msg_isr_14

msg_isr_16: db 'Error Matematico en la FPU'
msg_isr_len_16 EQU $ - msg_isr_16

msg_isr_17: db 'Chequeo de alineacion de memoria'
msg_isr_len_17 EQU $ - msg_isr_17

msg_isr_18: db 'Machine Check'
msg_isr_len_18 EQU $ - msg_isr_18

msg_isr_19: db 'Eror en calculo SIMD con variables de Punto Flotante'
msg_isr_len_19 EQU $ - msg_isr_19

msg_isr_33: db 'Teclado!!!'
msg_isr_len_33 EQU $ - msg_isr_33

tecla: db ' '

global get_eip

get_eip:
	mov eax, esp
	ret

;;
;; Definición de MACROS
;; -------------------------------------------------------------------------- ;;

%macro ISR 1
global _isr%1


_isr%1:
	pushad
	pushfd
	str cx
	shl cx, 3
	cmp cx, 13
	jbe .noSonPerros
		cmp byte [modo_juego], 0
		je .iddle
		mov byte [modo_juego], 2
		mov eax, ss
		push eax
		mov eax, fs
		push eax
		mov eax, ds
		push eax
		mov eax, cs
		push eax
		mov eax, es
		push eax
		mov eax, gs
		push eax
		call game_modo_debug
		add esp, 4*6
		imprimir_texto_mp msg_isr_%1, msg_isr_len_%1, 0x07, 1, 35
		.iddle:
		call sched_tarea_actual
		push eax
		call game_perro_termino
		add esp, 4
		jmp 0x98:0
	.noSonPerros:
	imprimir_texto_mp msg_isr_%1, msg_isr_len_%1, 0x07, 0, 0
	.fin:
	popfd
	popad
	iret

%endmacro

;;
;; Datos
;; -------------------------------------------------------------------------- ;;
; Scheduler

;;
;; Rutina de atención de las EXCEPCIONES
;; -------------------------------------------------------------------------- ;;
ISR 0
ISR 1
ISR 2
ISR 3
ISR 4
ISR 5
ISR 6
ISR 7
ISR 8
ISR 10
ISR 11
ISR 12
ISR 13
ISR 14
ISR 16
ISR 17
ISR 18
ISR 19


;;
;; Rutina de atención del RELOJ
;; -------------------------------------------------------------------------- ;;
global _isr32

_isr32:
	pushad
	pushf
    call fin_intr_pic1
	call sched_atender_tick
	str cx
	cmp ax, cx
	je .salteo
	mov word [sched_tarea_selector], ax
	mov dword [sched_tarea_offset], 0x0
	jmp far [sched_tarea_offset]
	.salteo:
    call fin_intr_pic1
	popf
	popad
	iret

;;
;; Rutina de atención del TECLADO
;; -------------------------------------------------------------------------- ;;
global _isr33

_isr33:
	pushad
	pushf
	xor eax, eax
	in al, 0x60
	push eax
	mov eax, tecla
	push eax
	call game_tecla
	imprimir_texto_mp tecla, 1, 0x07, 0, 78
	pop eax
	call game_atender_teclado
	pop eax
    call fin_intr_pic1
	popf
	popad
	iret
	

;;
;; Rutinas de atención de las SYSCALLS
;; -------------------------------------------------------------------------- ;;

global _isr70

_isr70:
	pushad
	pushf
	push ecx
	push eax
	call game_syscall_manejar
	mov [syscall_retorno], eax
	add esp, 8
	popf
	popad
	mov eax, [syscall_retorno]
	iret
