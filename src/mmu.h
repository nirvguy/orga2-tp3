/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del manejador de memoria
*/

#ifndef __MMU_H__
#define __MMU_H__

#include "defines.h"
#include "game.h"

typedef struct {
	unsigned int attr:12; // DISP (3) | G | PS | 0 | A | PCD | PWT | U/S | R/W | P
	unsigned int dir_base:20;
} __attribute__((__packed__)) str_pd_entry, str_pt_entry;


typedef struct {
	unsigned short offset:12;
	unsigned short table:10;
	unsigned short directory:10;
} __attribute__((__packed__)) dir_lineal;

typedef struct {
	unsigned int f_ignored:3;
	unsigned char cache_attr:2; // PCD | PWT
	unsigned int s_ignored:7;
	unsigned int dir_base_pd:20;
} __attribute__((__packed__)) CR3;

int offset_sig_pag_libre;
void* pagina_tarea_compartida;

void mmu_inicializar();

void* dame_pagina_vacia();

// devuelve la proxima pagina libre del area libre del kernel
uint mmu_proxima_pagina_fisica_libre();

void mapping_kernel(str_pt_entry* pte, uint attr, uint offset);

// setea en cero todos los bytes
void mmu_inicializar_pagina(uint * pagina);

// copia los bytes
void mmu_copiar_pagina    (uint src, uint dst);

// pide una pagina para usar de directorio. Luego inicializa las entradas que iran con identity mapping.
uint mmu_inicializar_dir_kernel();

// transforma coordenadas (x,y) en direcciones fisicas
uint mmu_xy2fisica(uint x, uint y);

// transforma coordenadas (x,y) en direcciones virtuales
uint mmu_xy2virtual(uint x, uint y);

// crea el directorio, las paginas, copia el codigo e inicializa el stack
uint mmu_inicializar_memoria_perro(perro_t *perro, int index_jugador, int index_tipo);

// debe remapear y copiar el codigo
void mmu_mover_perro(perro_t *perro, int viejo_x, int viejo_y);


void mmu_mapear_pagina  (uint virtual, uint cr3, uint fisica, uint attrs);
void mmu_unmapear_pagina(uint virtual, uint cr3);
void mmu_remapear_perro(perro_t* perro);


#endif	/* !__MMU_H__ */
