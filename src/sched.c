/* ** por compatibilidad se omiten tildes **
================================================================================
TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
definicion de funciones del scheduler
*/

#include "sched.h"
#include "i386.h"
#include "screen.h"

sched_t scheduler;

void sched_inicializar()
{
	scheduler.current = 0;
	scheduler.perro_other = 0;
	scheduler.jugador_current = 2;
	scheduler.tasks[0].gdt_index = SEL_GDT_TSS_IDLE_ENTRY;
	scheduler.tasks[0].perro = &perro_iddle;
	perro_iddle.libre = TRUE;
	perro_iddle.jugador = &jugadorA;
	int i=1;
	for(;i<MAX_CANT_TAREAS_VIVAS+1;i++) {
		scheduler.tasks[i].gdt_index = (i +GDT_TSS_IDLE_ENTRY) << 3;
		if(i < 9)
			scheduler.tasks[i].perro = &(jugadorA.perros[i-1]);
		else
			scheduler.tasks[i].perro = &(jugadorB.perros[i-9]);
	}
}


int sched_buscar_indice_tarea(uint gdt_index) {
    return gdt_index-(GDT_TSS_IDLE_ENTRY+1);
}


void sched_matar_actual() {
	scheduler.current = 0;
    __asm __volatile("jmp $0x68, $0x0" : : );
    /* __asm __volatile("xchg %%bx, %%bx" : :); */

}


perro_t* sched_tarea_actual()
{
	switch(scheduler.jugador_current) {
		case 0:
			return scheduler.tasks[scheduler.current+1].perro;
		case 1:
			return scheduler.tasks[scheduler.current+9].perro;
		default:
			return &perro_iddle;
	}
}


uint sched_proxima_a_ejecutar()
{
	int jugador = ((scheduler.jugador_current == 0) ? 8 : 0);
	int i = (scheduler.perro_other+1) % 8;
	while (i != scheduler.perro_other && (scheduler.tasks[i+jugador+1].perro)->libre)
		i = (i+1) % MAX_CANT_PERROS_VIVOS;
	if(i != scheduler.perro_other || !((scheduler.tasks[i+jugador+1].perro)->libre)) {
		return i+jugador+1;
	}
	jugador = ((scheduler.jugador_current == 0) ? 0 : 8);
	i = (scheduler.current+1) % 8;
	while (i != scheduler.current && (scheduler.tasks[i+jugador+1].perro)->libre)
		i = (i+1) % MAX_CANT_PERROS_VIVOS;
    if(i != scheduler.current || !((scheduler.tasks[i+jugador+1].perro)->libre))
		return i+jugador+1;
	return 0;
}

ushort sched_atender_tick()
{
	perro_t* p_act = sched_tarea_actual();
	if(modo_juego == MATAR_PERRO_DEBUG) {
		return SEL_GDT_TSS_IDLE_ENTRY;
	}
	game_atender_tick(sched_tarea_actual());
    uint prox = sched_proxima_a_ejecutar();
	if(prox != 0) {
		scheduler.jugador_current = ((scheduler.tasks[prox].perro)->jugador)->index;
		scheduler.perro_other = p_act->index;
		scheduler.current = (scheduler.tasks[prox].perro)->index;
		return scheduler.tasks[prox].gdt_index;
	} else {
		return SEL_GDT_TSS_IDLE_ENTRY;
	}
}


