MAKE=make

KERNEL_DIR = src


.PHONY: all clean KERNEL

all: KERNEL

KERNEL:
	cd $(KERNEL_DIR) && $(MAKE)
	
clean:
	cd $(KERNEL_DIR) && $(MAKE) clean
	


